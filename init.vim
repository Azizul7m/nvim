""Plug
call plug#begin('~/.vim/pluggedNvim/')

    if has ("nvim") 
        Plug 'romgrk/barbar.nvim'
        Plug 'nvim-lua/plenary.nvim' "A Lua module for asynchronous programming using coroutines.
        "lsp
        Plug 'neovim/nvim-lspconfig'
        Plug 'williamboman/nvim-lsp-installer'
        Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
        Plug 'hrsh7th/nvim-compe'
        Plug 'hrsh7th/cmp-nvim-lsp'
        Plug 'hrsh7th/cmp-buffer'
        Plug 'hrsh7th/cmp-path'
        Plug 'hrsh7th/cmp-cmdline'
        Plug 'hrsh7th/nvim-cmp'
        Plug 'https://github.com/hrsh7th/cmp-copilot.git'
        " For vsnip users.
        Plug 'L3MON4D3/LuaSnip'
        Plug 'saadparwaiz1/cmp_luasnip'
        Plug 'rafamadriz/friendly-snippets'

        "Coc
        " Plug 'neoclide/coc.nvim', {'branch': 'release'}

        Plug 'thaerkh/vim-indentguides'
        Plug 'nvim-telescope/telescope.nvim'
        Plug 'ahmedkhalf/project.nvim'
        Plug 'https://github.com/preservim/tagbar.git' "Tagbar all function set form there;
        Plug 'mattn/emmet-vim'
        Plug 'ap/vim-css-color'
        Plug 'tpope/vim-commentary'
        Plug 'folke/todo-comments.nvim' "Todo comments
        Plug 'https://tpope.io/vim/surround.git' " Tag Changing
        Plug 'jiangmiao/auto-pairs'
        Plug 'https://github.com/Valloric/MatchTagAlways.git' " Matching Tag

        Plug 'nvim-lualine/lualine.nvim'
        Plug 'kyazdani42/nvim-tree.lua'

        Plug 'https://github.com/github/copilot.vim.git' " github copilot

        " Browser Sync
        Plug 'tamago324/vim-browsersync'
        Plug 'tyru/open-browser.vim'
        endif
    "themes staff
    Plug 'navarasu/onedark.nvim'
    Plug 'catppuccin/nvim', {'as': 'catppuccin'}

    "Utils
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'terryma/vim-multiple-cursors'
    Plug 'https://github.com/rcarriga/nvim-notify.git'
    Plug 'akinsho/toggleterm.nvim'
    Plug 'glepnir/dashboard-nvim'
    Plug 'wfxr/minimap.vim'
    Plug 'folke/which-key.nvim'
    "ranger
    Plug 'francoiscabrol/ranger.vim'
    Plug 'rbgrouleff/bclose.vim' " Dependency for ranger
    Plug 'liuchengxu/vim-clap' " Find or dispatch anything on the fly, with smart cache strategy.
    "git
    Plug 'tpope/vim-fugitive' "Fugitive is the premier Vim plugin for Git
    Plug 'lewis6991/gitsigns.nvim'
    Plug 'sheerun/vim-polyglot' " Polyglot is a plugin for Vim that allows you to syntax highlight.
    

    " post install (yarn install | npm install) then load plugin only for editing supported files
    Plug 'prettier/vim-prettier', {
      \ 'do': 'yarn install --frozen-lockfile --production',
      \ 'for': ['javascript', 'php' ,'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'svelte', 'yaml', 'html'] }
call plug#end()



"themes Staff
"--------------------------------------------------------------
" colorscheme gruvbox8
colorscheme onedark
if !exists("g:onedark_terminal_italics")
  let g:onedark_terminal_italics = 1
endif


"Random Staff
"----------------------------------------------------------------
let g:mucomplete#enable_auto_at_startup = 1
let g:mapleader = "\<Space>"
inoremap <expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr> <S-TAB> pumvisible() ? "\<C-p>" : "\<TAB>"


autocmd BufWritePost *.php silent! call PhpCsFixerFixFile()


"Commtentary
" nnoremap <space>/ :Commentary<CR>
vnoremap <space>/ :Commentary<CR>
nmap <F8> :TagbarToggle<CR>
nmap <F7> :MinimapToggle<CR>



" Ranger
let g:ranger_map_keys = 0
let g:ranger_command_override = 'ranger --cmd "set show_hidden=true"'

" let g:UltiSnipsExpandTrigger=","


"Mutli Cursor
let g:multi_cursor_use_default_mapping=0
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)



" Default mapping
 let g:multi_cursor_start_word_key      = '<C-n>'
 let g:multi_cursor_select_all_word_key = '<A-n>'
" let g:multi_cursor_start_key           = 'g<C-n>'
" let g:multi_cursor_select_all_key      = 'g<A-n>'
 let g:multi_cursor_next_key            = '<C-n>'
 let g:multi_cursor_prev_key            = '<C-p>'
 let g:multi_cursor_skip_key            = '<C-x>'
 let g:multi_cursor_quit_key            = '<Esc>'




"rainbow
let g:rainbow_active = 1
let g:rainbow_load_separately = [
    \ [ '*' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.tex' , [['(', ')'], ['\[', '\]']] ],
    \ [ '*.cpp' , [['(', ')'], ['\[', '\]'], ['{', '}']] ],
    \ [ '*.{html,htm}' , [['(', ')'], ['\[', '\]'], ['{', '}'], ['<\a[^>]*>', '</[^>]*>']] ],
    \ ]

let g:rainbow_guifgs = ['RoyalBlue3', 'DarkOrange3', 'DarkOrchid3', 'FireBrick']
let g:rainbow_ctermfgs = ['lightblue', 'lightgreen', 'yellow', 'red', 'magenta']


"indentguides
let g:indentguides_spacechar = '┆'
let g:indentguides_tabchar = '|'
let g:indentguides_ignorelist = ['text']


"emmet
let g:user_emmet_leader_key=','
filetype plugin on

" Set the completefunc you can do this per file basis or with a mapping
" set completefunc=tailwind#complete

" The Mapping I use
" nnoremap <leader>tt :set completefunc=tailwind#complete<cr>
" Add this autocmd to your vimrc to close the preview window after the completion is done
" autocmd CompleteDone * pclose


"Coc

let g:coc_global_extensions = [
    \ 'coc-css',
    \ 'coc-diagnostic'
    \]

inoremap <silent><expr> <c-space> coc#refresh()



autocmd ColorScheme *
  \ hi CocExplorerNormalFloatBorder guifg=#414347 guibg=#272B34
  \ | hi CocExplorerNormalFloat guibg=#272B34
  \ | hi CocExplorerSelectUI guibg=blue


"Dashboard

"markdown
let g:mkdp_browser = 'firefox'

let g:mkdp_auto_start = 1
let g:mkdp_open_ip = '127.0.0.1:8080'

"devicons
let g:webdevicons_enable = 1
let g:webdevicons_enable_nerdtree = 1
let g:webdevicons_enable_unite = 1


lua << END
    require("notify")
    require("init")
    vim.notify = require("notify")
END



"
"Clap
let g:clap_layout = { 'relative': 'editor' }




" vim-prettier
let g:prettier#quickfix_enabled = 1
let g:prettier#quickfix_auto_focus = 1
run prettier on save
autocmd BufWritePre  *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.svelte,*.yaml,*.html,*.php PrettierAsync
let g:prettier#config#tab_width = '2'
let g:prettier#config#parser = ''






nmap <Tab> :tabnext<CR>
nmap <S-Tab> :tabprev<CR>



"KeyBinding By Azizul7m
"-----------------------------------------------------------------"



let mapleader = " "
inoremap jk <ESC>

"markdown
nmap <leader>mp <Plug>MarkdownPreview
nmap <leader>ms <Plug>MarkdownPreviewStop
nmap <leader>mt <Plug>MarkdownPreviewToggle


"Dashboard
nmap <leader>ss :<C-u>SessionSave<CR>
nmap <leader>sl :<C-u>SessionLoad<CR>






"resize buffer window with arrow

nnoremap <silent> <C-Up> :resize +2<CR>
nnoremap <silent> <C-down> :resize -2<CR>
nnoremap <silent> <C-left> :vertical resize +2<CR>
nnoremap <silent> <C-right> :vertical resize -2<CR>

"Batter window Navigation
nnoremap <C-h> <C-\><C-N><C-w>h
nnoremap <C-j> <C-\><C-N><C-w>j
nnoremap <C-k> <C-\><C-N><C-w>k
nnoremap <C-l> <C-\><C-N><C-w>l

" Automatically fix the last misspelled word and jump back to where you were.
"   Taken from this talk: https://www.youtube.com/watch?v=lwD8G1P52Sk
nnoremap <leader>sp :normal! mz[s1z=`z<CR>


" Move 1 more lines up or down in normal and visual selection modes.

"nnoremap <C-k> :m .-2<CR>==
"nnoremap <C-j> :m .+1<CR>==
"nnoremap <C-Up> :m .-2<CR>==
"nnoremap <C-Down> :m .+1<CR>==
vnoremap <C-k> :m '<-2<CR>gv=gv
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-Down> :m '>+1<CR>gv=gv
vnoremap <C-Up> :m '<-2<CR>gv=gv


"completation
"____________________________________________________________________

" Navigate the complete menu items like CTRL+n / CTRL+p would.
inoremap <expr> <Down> pumvisible() ? "<C-n>" :"<Down>"
inoremap <expr> <Up> pumvisible() ? "<C-p>" : "<Up>"

" Select the complete menu item like CTRL+y would.
inoremap <expr> <Right> pumvisible() ? "<C-y>" : "<Right>"
inoremap <expr> <CR> pumvisible() ? "<C-y>" :"<CR>"
" Cancel the complete menu item like CTRL+e would.
inoremap <expr> <Left> pumvisible() ? "<C-e>" : "<Left>"




"autocmd

"NERDTree 
" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1






"
"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
""""Setting


"core setting by azizul Islam
syntax on
set ruler
set colorcolumn=335
set number
"set exrc   "Alternative of vimrc
set nobackup
set noswapfile
set incsearch
set ttyfast
set scrolloff=5
set incsearch "Do incremental searching
set splitbelow "window Decoretion buffer Horizontal open in blew
set splitright  "window Decoretion
set cursorline
" set colorcolumn
set shortmess+=c
set nowrap
set relativenumber
set mouse=a
set smarttab
set cindent
set tabstop=4
set shiftwidth=2
set foldcolumn=1
" set pumheight = 10 "Makes Popup menu smaller
set signcolumn=no "Side space for git status someting like that
set updatetime=300
set smartindent
set expandtab smarttab
set formatoptions=tcqrn1
set complete+=kspell
set complete-=nofold
set mps+=<:> "Matching pairs
set timeoutlen=500 "Timeout for command
" set wildmode=longest,list,full
" set emo

" hi NORMAL  guibg=NONE


"Utils
set dictionary+=/usr/share/dict/words
set complete+=k/usr/share/dict/words
set dict= "dictionary
set sps=best
set icon
set clipboard=unnamedplus
set ph=0 "pumheight
set pw=15 "pumwidth
set ofu= "omnifunc
set sm "showmatch
let g:netrw_winsize=20 "netrw window size in lines  

set undodir=/tmp
set directory=/tmp//,.
